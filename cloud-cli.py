#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Cloud Environment Manager.

Usage:
  cloud-cli.py new <name> [--base-env=<env>] [--repo=<repo>]
  cloud-cli.py init <name>
  cloud-cli.py (-h | --help)

Options:
  --base-env=<env> Branch from which to fork [Default: qa]
  --repo=<repo> Repository to use
                [Default: git@bitbucket.org:arxantech/cloud-ci.git]
  -h --help     Show this screen.

"""
from docopt import docopt

import tempfile
import shutil
import os
import sys
import yaml
import re

from sh import git, head


def ask(question, default="", can_be_empty=False):
    while True:
        value = raw_input("{q}? [{d}] \n".format(q=question, d=default))
        if not value:
            if not default and not can_be_empty:
                print("The provided value can not be empty. Try again.")
                continue

            print("Using default value: {}".format(default))
            value = default
        return value

def sed_inplace(filename, pattern, repl):
    '''
    Perform the pure-Python equivalent of in-place `sed` substitution: e.g.,
    `sed -i -e 's/'${pattern}'/'${repl}' "${filename}"`.
    '''
    # For efficiency, precompile the passed regular expression.
    pattern_compiled = re.compile(pattern)

    # For portability, NamedTemporaryFile() defaults to mode "w+b" (i.e., binary
    # writing with updating). This is usually a good thing. In this case,
    # however, binary writing imposes non-trivial encoding constraints trivially
    # resolved by switching to text writing. Let's do that.
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as tmp_file:
        with open(filename) as src_file:
            for line in src_file:
                tmp_file.write(pattern_compiled.sub(repl, line))

    # Overwrite the original file with the munged temporary file in a
    # manner preserving file attributes (e.g., permissions).
    shutil.copystat(filename, tmp_file.name)
    shutil.move(tmp_file.name, filename)

def ask_and_update(question, variable, env_file, config_values):
    value = ask(question, default=config_values.get(variable))
    sed_inplace(env_file, "{var}: .*".format(var=variable),
                          "{var}: {value}".format(var=variable, value=value))

if __name__ == "__main__":
    arguments = docopt(__doc__, version='cloud-cli 1.0')

    if arguments["new"]:
        if os.path.exists(arguments["--base-env"] + ".yaml"):
            base_file = os.path.join(os.getcwd(), arguments["--base-env"] + ".yaml")
        elif os.path.exists(os.path.join(os.getcwd(), "vars", arguments["--base-env"] + ".yaml")):
            base_file = os.path.join(os.getcwd(), "vars", arguments["--base-env"] + ".yaml")
        else:
            print("Can not find ansible vars directory. Run the script from the \
                   root of initialized project, or ansible vars directory.")
            exit(1)

        new_file = os.path.join(os.path.split(base_file)[0], arguments["<name>"] + ".yaml")
        shutil.copy(base_file, new_file)

        with file(new_file, "r") as f:
            config_values = yaml.load(f)

        sed_inplace(new_file, "env_name: .*", "env_name: {}".format(arguments["<name>"]))


        print("Now we'll setup domains for services.\n↓")
        ask_and_update("Whats the domain name for backend", "backend_domain", new_file, config_values)
        ask_and_update("Whats the domain name for frontend", "frontend_domain", new_file, config_values)

        print("_\n↓\nNow we'll define branches/revisions deployed in services.\n↓")
        ask_and_update("Whats the branch/revision for Backend", "backend_revision", new_file, config_values)
        ask_and_update("Whats the branch/revision for Frontend", "frontend_revision", new_file, config_values)
        ask_and_update("Whats the branch/revision for APaaS", "apaas_branch", new_file, config_values)
        ask_and_update("Whats the branch/revision for Telemetry", "telemetry_revision", new_file, config_values)
        ask_and_update("Whats the branch/revision for OAuth", "oauth_revision", new_file, config_values)

    elif arguments["init"]:
        os.mkdir(os.path.join(os.getcwd(), arguments["<name>"]))
        os.chdir(arguments["<name>"])
        print("Cloning repositories...")
        print("Global configuration...")
        git("clone", "git@bitbucket.org:arxantech/arxan_global_configuration.git", "vars", _out=sys.stdout)
        print("Centralised roles...")
        git("clone", "git@bitbucket.org:arxantech/arxan_ansible_roles.git", "roles", _out=sys.stdout)
        os.mkdir("apps")
        os.chdir("apps")
        print("ADP-ci...")
        git("clone", "git@bitbucket.org:arxantech/cloud-ci.git", _out=sys.stdout)
        print("APaaS-ci...")
        git("clone", "git@bitbucket.org:arxantech/cloud-apaas-protect-ci.git", _out=sys.stdout)
        print("Auth-ci...")
        git("clone", "git@bitbucket.org:arxantech/cloud-auth-service-ci.git", _out=sys.stdout)
        print("License platform ci...")
        git("clone", "git@bitbucket.org:arxantech/cloud-license-platform-ci.git", _out=sys.stdout)
